curl -o ~/bin/magerun2 https://files.magerun.net/n98-magerun2.phar && chmod +x ~/bin/magerun2

curl -o ~/bin/mysql_backups.sh https://bitbucket.org/ponbikedmp/magento-cloud-deploy/raw/master/mysql_backup.sh && chmod +x ~/bin/mysql_backups.sh

curl -o ~/bin/sync_psh.sh https://bitbucket.org/ponbikedmp/magento-cloud-deploy/raw/master/sync_psh.sh && chmod +x ~/bin/sync_psh.sh

chmod +x ~/bin/magento

echo "alias m2='~/bin/magerun2'" >> ~/.bash_profile
echo "alias ll='ls -la'" >> ~/.bash_profile
