#!/bin/sh
DBLOCATION="$MAGENTO_CLOUD_APP_DIR/var/mysql_backups"
if [ ! -d "$DIR" ]; then
    mkdir -p $DBLOCATION
fi
DATE=`date +%u`
FULLDB="$DBLOCATION/full-db-$DATE.sql"
STRIPDB="$DBLOCATION/stripped-db-$DATE.sql"
STRIPOPTIONS="@trade @customers @log webforms_files webforms_message webforms_results webforms_results_values pon_mybikes purchasevideo_queue order_export_queue"

echo "Starting FULL DB dump to $FULLDB"
~/bin/magerun2 db:dump -c gz $FULLDB
ln -sf $FULLDB $DBLOCATION/full-db-latest.sql.gz

echo "Starting Stripped DB Dump to $STRIPDB"
~/bin/magerun2 db:dump -c gz --strip="$STRIPOPTIONS" $STRIPDB
ln -sf $STRIPDB $DBLOCATION/stripped-db-latest.sql.gz