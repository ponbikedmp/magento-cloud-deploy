#!/usr/bin/env bash
GIT_SSH_COMMAND='ssh -o "SendEnv PLATFORMSH_PUSH_NO_WAIT"'

export PLATFORMSH_PUSH_NO_WAIT=$PLATFORMSH_PUSH_NO_WAIT
REMOTE_GIT_URL="$1"
REMOTE_BRANCH="$2"
BITBUCKET_TAG=`git describe --tags`
BITBUCKET_CURRENT_BRANCH=`git branch | grep \* | cut -d ' ' -f2`

echo "======================================================================================================"
echo "Starting deployment:"
echo " - Project                  : $BITBUCKET_REPO_SLUG"
echo " - Bitbucket branch         : $BITBUCKET_BRANCH"
echo " - Current branch           : '$BITBUCKET_CURRENT_BRANCH'"
echo " - Last commit ID           : $BITBUCKET_COMMIT"
echo " - Tag                      : $BITBUCKET_TAG"
echo " - Remote GIT Url           : $REMOTE_GIT_URL"
echo " - Remote branch            : $REMOTE_BRANCH"
echo " - Build home directory     : $HOME"
echo " - Build project directory  : $BITBUCKET_CLONE_DIR"
echo "======================================================================================================"
echo "Current status:"

if [ "$REMOTE_GIT_URL" == "" ]; then
    echo "INVALID REMOTE GIT URL SPECIFIED!"
    exit 1
fi

if [ "$REMOTE_BRANCH" == "" ]; then
    REMOTE_BRANCH="$BITBUCKET_BRANCH"
fi

if [ "$RENAME_BRANCH" != "skip" ]; then
    if [ "$REMOTE_BRANCH" == "master" ]; then
        REMOTE_BRANCH="Production"
    fi


    if [ "$REMOTE_BRANCH" == "uat" ]; then
        REMOTE_BRANCH="Staging"
    fi
fi

if [ "$REMOTE_BRANCH" == "" ]; then
    echo "UNABLE TO PUSH FROM COMMIT/TAG!"
    exit 1
fi

# Environment setup (SSH)
if [ ! -d "$HOME/.ssh" ]; then
    echo -n " - Creating .ssh directory         : "
    mkdir -p $HOME/.ssh
    echo "[DONE]";
fi

# Set ssh config
echo "Host *" >> $HOME/.ssh/config
echo "  SendEnv PLATFORMSH_PUSH_NO_WAIT" >> $HOME/.ssh/config

# Add RequireJS optimizer script to bundle JS files
if [ -f build.json ]; then
    echo -n " - Adding RequireJS optimizer      : "
    mv tools/optimizer.sh ./
    chmod +x optimizer.sh
    git add -f optimizer.sh
    echo "[DONE]";
fi

#Create release information
echo -n "Build: "$BITBUCKET_REPO_SLUG"/"$BITBUCKET_BRANCH"/"$BITBUCKET_TAG" Commit: "$BITBUCKET_COMMIT" Date: " > release_build
date >> release_build
git add -f release_build

# Set git user information
git config --global user.email "magento@pon.bike"
git config --global user.name "Pon Bike Deployment"

# Push to remote git repository
echo " - Deploying project     : "
echo "   - Adding Magento Cloud as a remote...."
git remote add mc $REMOTE_GIT_URL
echo "   - Commit deployment files...."
git commit -m "Added deployment files"
echo "   - Repacking git repository"
git repack
echo "   - Push to Magento Cloud"
git push -f mc $BITBUCKET_BRANCH:$REMOTE_BRANCH
if [[ $? -ne 0 ]] ; 
then
    echo " - Magento.cloud project deployment failed!"
    exit -1
fi
echo " - Project deployment done!"
