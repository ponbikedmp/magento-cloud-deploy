#!/bin/bash
REMOTE_HOST=$1

if [[ "$REMOTE_HOST" == "" ]]; then
    REMOTE_HOST=$PLATFORM_SSH
fi

rsync -avzh $REMOTE_HOST:private/ private/
rsync -avzh $REMOTE_HOST:pub/media/ pub/media/